#Josh Churchill
#Final Exam Class
import math
class Circle2D:

  
  def __init__(self, x, y, radius):
    self.__x = x 
    self.__y = y
    self.__radius = radius
  
 
  def get_x(self):
    return self.__x 
  
  def get_y(self):
    return self.__y
  
  def get_radius(self):
    return self.__radius 
    
  def set_x(self,x):
    self.__x = x 
  
  def set_y(self,y):
    self.__y = y
  
  def set_radius(self,radius):
    self.__radius = radius 
  
  
  def getArea(self):
    area = math.pi * math.pow(self.get_radius(), 2)
    return area

  def getPerimeter(self):
    perimeter = 2 * math.pi * self.get_radius()
    return perimeter

  def containsPoint(self):
    xVal = float(input("Please enter the x position of the point "))
    yVal = float(input("Please enter the y position of the point "))
  
    if math.fabs(xVal - self.get_x()) > self.get_radius():
      if math.fabs(yVal - self.get_y()) > self.get_radius():
       #finString = "Point " + "(" + str(xVal) + "," + str(yVal) + ")" + " is not in the circle"
       #print(finString)
       return False
    else:
      #finString = "Point " + "(" + str(xVal) + "," + str(yVal) + ")" + " is in the circle"
      #print(finString)
      return True

  def contains(self):
    xVal = float(input("Please enter the x position of the center of your new circle "))
    yVal = float(input("Please enter the y position of the center of your new circle "))
    radVal = float(input("Please enter the radius of the center of your new circle "))
    if math.fabs(xVal - self.get_x()) > self.get_radius() + radVal:
      if math.fabs(yVal - self.get_y()) > self.get_radius() + radVal:
        #print("Your new circle is not inside your original circle")
        return False
    else:
      #print("Your new circle is inside your original circle")
      return True

  def overlaps(self):
    xVal = float(input("Please enter the x position of the center of your new circle "))
    yVal = float(input("Please enter the y position of the center of your new circle "))
    radVal = float(input("Please enter the radius of the center of your new circle "))
    if math.fabs(xVal - self.get_x()) > self.get_radius() + radVal:
      if math.fabs(yVal - self.get_y()) > self.get_radius() + radVal:
        #print("Your new circle does not overlap inside your original circle")
        return False
    else:
      #print("Your new circle overlaps inside your original circle")
      return True
      
  
      
  
  
  
  