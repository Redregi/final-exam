#Josh Churchill
#Final Exam Test
#addLibPath("/Users/jchurchill2016/Desktop/FinalExam/")
import math
from circle2d import *
n = 0 
circle = Circle2D(0,0,0)

xVal = float(input("Please enter the x position of the center of your circle "))
yVal = float(input("Please enter the y position of the center of your circle "))
radVal = float(input("Please enter the radius of the center of your circle "))

circle.set_x(xVal)
circle.set_y(yVal)
circle.set_radius(radVal)



  

def options():
  global xVal
  global yVal
  global circle
  print("")
  print("============================")
  print("1. Get the area of your circle")
  print("2. Get the perimeter of your circle")
  print("3. Check if a point is contained within your circle ")
  print("4. Check if a circle is contained within your circle")
  print("5. Check if a circle overlaps with your circle")
  print("")
  choice = int(input("Please select an option "))
  if choice == 1:
    print("")
    circle.getArea()
    print(circle.getArea())
    print("")
  if choice == 2:
    print("")
    circle.getPerimeter()
    print(circle.getPerimeter())
    print("")
  if choice == 3:
    print("")
    if circle.containsPoint():
      print("Your point is inside of your circle")
    else:
      print("Your point is not inside of your circle")
    print("")
  if choice == 4:
    print("")
    if circle.contains():
      print("Your new circle is inside of your original circle")
    else:
      print("Your new circle is not inside of your original circle")
    print("")
  if choice == 5:
    print("")
    if circle.overlaps():
      print("Your new circle overlaps with your original circle")
    else:
      print("Your new circle does not overlap with your original circle")
    print("")
  options()

options()

#circle.getArea()
#print(circle.getArea())
#circle.getPerimeter()
#print(circle.containsPoint())
#print(circle.contains())
#print(circle.overlaps())


  
  
